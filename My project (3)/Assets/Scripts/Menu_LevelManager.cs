using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Menu_LevelManager : MonoBehaviour
{

    public void ChangeToLevel1()
    {
        SceneManager.LoadSceneAsync("MemoryCardGame");
    }
    
    public void ChangeToLevel2()
    {

        SceneManager.LoadSceneAsync("ReflexTester");
    }
    
    public void ChangeToLevel3()
    {
   
        SceneManager.LoadSceneAsync("Menu");
    }
    
    public void ChangeToLevel4()
    {
        SceneManager.LoadSceneAsync("SampleScene");
    }
}
