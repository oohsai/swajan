
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainLevelManager : MonoBehaviour
{
    public void GotoPlay()
    {
        SceneManager.LoadSceneAsync("make");
    }
    public void GotoLogin()
    {
        SceneManager.LoadSceneAsync("auth_login");
    }
    public void GotoRegister()
    {
        SceneManager.LoadSceneAsync("auth_register");
    }
    public void QuitGame()
    {
        Application.Quit();
    }

}
